-module(gpop).
%%% Google Popular terms 
%%% Feed URL: http://www.google.com/trends/hottrends/atom/hourly
%%% @author Dylan Hutchison <dhutchis@stevens.edu>

-include_lib("emp_core/include/emputil.hrl").
-include_lib("emp_core/include/emp_typedefs.hrl").

-type html() :: string().
-type gterm() :: string().
-define(NEW_TREND_EVENT, <<"new_trend">>).
-define(POLL_INTERVAL, (3600*1000)). % in ms
-compile(export_all). %debug...

-behaviour(emp_plugin).
%%emp_plugin exports
-export([registration_info/0, 
	 register_cmds/0,     %required
	 register_events/0,   %required
	 init/1,     % optional
	 finish/1]).
%% Command exports
-export([update_now/1, poll_trends/0]).
%% Module exports

%%% State Variables:
%%% trends -> [gterm()] The last polled trends
%%% poll_loop_pid -> pid() The spawned proc that does the polling.

registration_info() ->
    {gpop, %"Google Popular Term Poll", 
     [{caching, false},
      {start_on_boot, false},
      {initial_cmd, fun do_poll_compare/1}, % ?
      {local_global, global},
      {sys_user, user}]}. % ?

register_events() ->
    [%"New Trend", {"New Trend", gterm(), ""}}].
     ?EVENT( ?NEW_TREND_EVENT, [?PARAM(<<"term">>, ?EMP_STRING, "")] )
    ].

register_cmds() ->
    [
     ?COMMAND(<<"update">>, fun update_now/0, [], ?EMP_BLANK),
     ?COMMAND(<<"get_current_trends">>, fun poll_trends/0, [], ?EMP_LIST(?EMP_STRING)) %[gterm()]].
    ].

init(StateId) ->
    inets:start(),
    %% Set trends to [] if no trends exist in state
    case emp_plugin:get_var(StateId, "trends") of
	Trends when is_list(Trends) -> ok;
	{error, _Reason} ->
	    emp_plugin:set_var(StateId, "trends", []) % Ignore error case
    end,
    %% Start poll_loop proc
    Poll_loop_pid = spawn(?MODULE, poll_loop, [StateId]),
    emp_plugin:set_var(StateId, "poll_loop_pid", Poll_loop_pid),
    ok.

finish(StateId) ->
    %% Kill poll_loop proc
    case emp_plugin:get_var(StateId, "poll_loop_pid") of
	{error, _Reason} -> ok;
	Poll_loop_pid when is_pid(Poll_loop_pid) ->
	    Poll_loop_pid ! shutdown
    end,    
    ok.  

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%% Commands %%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%-spec update_now(StateId) -> ok | Error.
update_now(StateId) ->
    case emp_plugin:get_var(StateId, "poll_loop_pid") of
	Poll_loop_pid when is_pid(Poll_loop_pid) -> Poll_loop_pid ! update_now,
						    ok;
	Error -> Error
    end.

%% % inets:start().
%% {ok, {{Version, 200, ReasonPhrase}, Headers, Body}} =
%%      httpc:request(get, {"http://www.erlang.org", []}, [], []).
%% Returns list of popular terms
-spec poll_trends() -> [gterm()].
poll_trends() ->
    {ok, {{_Version, 200, _ReasonPhrase}, _Headers, Body}} =
	httpc:request(get, {"http://www.google.com/trends/hottrends/atom/hourly", []}, [], []),
    parse_body(Body).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%% Internal API %%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Polls Google for new trends every hour and sends out events.
%% Stops when sent shutdown. Updates immediately when sent update_now.
poll_loop(StateId) ->
    do_poll_compare(StateId),
						%send_after(?POLL_INTERVAL, self(), {poll_time})
    receive
	shutdown -> ok;
	update_now -> poll_loop(StateId)
    after ?POLL_INTERVAL ->
	    poll_loop(StateId)
    end.

%% do_poll_compare polls Google for current trends, compares to old trends in state,
%% and sends events on each new trend
%-spec do_poll_compare(StateId) -> ok.
do_poll_compare(StateId) ->
    CurTrends = poll_trends(),
    PrevTrends = emp_plugin:get_var(StateId, "trends"),
    NewTrends = CurTrends -- PrevTrends,
    CurTrends = emp_plugin:set_var(StateId, "trends", CurTrends), % bad set will not match
    announce_new_trends(StateId, NewTrends).

announce_new_trends(StateId, NewTrends) ->
    lists:foreach(fun(NewTrend) ->
			  emp_plugin:trigger_event(StateId, "New Trend", NewTrend)
		  end, NewTrends).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Parsing functions

-spec parse_body(Body :: html()) -> [gterm()].
parse_body(Body) ->
    parse_body(Body, []).

%% Found the beginning of a term line
%% <a href="http://www.google.com/trends/hottrends#a=20120720-Tom%2BHardy">Tom Hardy</a>
-spec parse_body(Body :: html(), AccTerms :: [gterm()]) -> [gterm()].
parse_body("<a " ++ Rest, AccTerms) ->
    {AfterLine, Term} = parse_term_line(Rest),
    parse_body(AfterLine, [Term | AccTerms]);
parse_body([_ | Rest], AccTerms) -> parse_body(Rest, AccTerms); % Switch order of this and next function?
parse_body([], AccTerms) -> lists:reverse(AccTerms).

%% Parse to the end of the '<a' link at '>' where term starts
-spec parse_term_line(Line :: html()) -> {AfterLine :: html(), Term :: gterm()}.
parse_term_line([$> | Rest]) -> parse_term(Rest);
parse_term_line([_ | Rest]) -> parse_term_line(Rest).

%% Accumulate from term beginning to end </a>
parse_term(TermStart) -> parse_term(TermStart, []).
parse_term("</a>" ++ Rest, AccTerm) -> {Rest, lists:reverse(AccTerm)};
parse_term([C | Rest], AccTerm) -> parse_term(Rest, [C | AccTerm]).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%% Tests %%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-ifdef(TEST).
-include_lib("eunit/include/eunit.hrl").

-define(GPOP_FEED_HTML,"<?xml version=\"1.0\" encoding=\"UTF-8\"?>

<feed xmlns=\"http://www.w3.org/2005/Atom\" version=\"1.0\" xml:lang=\"en\">
<title type=\"text\">Google Hot Trends</title>
<subtitle type=\"text\">What are people searching for on Google today?</subtitle>
<id>http://www.google.com/trends/hottrends/atom/hourly,2007-08</id>
<link href=\"http://www.google.com/trends/hottrends/atom/hourly\" rel=\"self\"/>
<updated>2012-07-22T09:00:00Z</updated>
<entry xmlns=\"http://www.w3.org/2005/Atom\">
<id>2012-07-22T09:00:00Z</id>
<title type=\"text\"><![CDATA[Usher, UFC, Jessica Ghawi, Columbine, News, ...]]></title>
<content type=\"html\"><![CDATA[
  <ol>
  <li><span class=\"Mild new\"><a href=\"http://www.google.com/trends/hottrends#a=20120721-Usher\">Usher</a></span></li>
  <li><span class=\"Medium new\"><a href=\"http://www.google.com/trends/hottrends#a=20120721-UFC\">UFC</a></span></li>
  <li><span class=\"Medium new\"><a href=\"http://www.google.com/trends/hottrends#a=20120720-Jessica%2BGhawi\">Jessica Ghawi</a></span></li>
  <li><span class=\"Mild new\"><a href=\"http://www.google.com/trends/hottrends#a=20120720-Columbine\">Columbine</a></span></li>
  <li><span class=\"Spicy new\"><a href=\"http://www.google.com/trends/hottrends#a=20120720-News\">News</a></span></li>
  <li><span class=\"Mild new\"><a href=\"http://www.google.com/trends/hottrends#a=20120720-British%2BOpen%2BLeaderboard\">British Open Leaderboard</a></span></li>
  <li><span class=\"Spicy new\"><a href=\"http://www.google.com/trends/hottrends#a=20120720-Tom%2BHardy\">Tom Hardy</a></span></li>
  <li><span class=\"On Fire new\"><a href=\"http://www.google.com/trends/hottrends#a=20120720-Aurora\">Aurora</a></span></li>
  <li><span class=\"On Fire new\"><a href=\"http://www.google.com/trends/hottrends#a=20120720-Colorado%2Bshooting\">Colorado shooting</a></span></li>
  <li><span class=\"On Fire new\"><a href=\"http://www.google.com/trends/hottrends#a=20120720-James%2BHolmes\">James Holmes</a></span></li>
  <li><span class=\"Medium new\"><a href=\"http://www.google.com/trends/hottrends#a=20120720-Batman\">Batman</a></span></li>
  <li><span class=\"Medium new\"><a href=\"http://www.google.com/trends/hottrends#a=20120719-Dwight%2BHoward\">Dwight Howard</a></span></li>
  <li><span class=\"Medium new\"><a href=\"http://www.google.com/trends/hottrends#a=20120719-Ramadan%2B2012\">Ramadan 2012</a></span></li>
  <li><span class=\"Spicy new\"><a href=\"http://www.google.com/trends/hottrends#a=20120719-Fred%2BWillard\">Fred Willard</a></span></li>
  <li><span class=\"Medium new\"><a href=\"http://www.google.com/trends/hottrends#a=20120719-Emmy%2Bnominations%2B2012\">Emmy nominations 2012</a></span></li>
  <li><span class=\"Spicy new\"><a href=\"http://www.google.com/trends/hottrends#a=20120719-Michelle%2BJenneke\">Michelle Jenneke</a></span></li>
  <li><span class=\"Spicy new\"><a href=\"http://www.google.com/trends/hottrends#a=20120719-British%2BOpen\">British Open</a></span></li>
  <li><span class=\"Medium new\"><a href=\"http://www.google.com/trends/hottrends#a=20120718-Chick%2BFil%2BA\">Chick Fil A</a></span></li>
  <li><span class=\"Mild new\"><a href=\"http://www.google.com/trends/hottrends#a=20120718-MC%2BChris\">MC Chris</a></span></li>
  <li><span class=\"Medium new\"><a href=\"http://www.google.com/trends/hottrends#a=20120718-AccuWeather\">AccuWeather</a></span></li>
  </ol>
]]>
</content>
</entry>

</feed>").

parse_body_test() ->
    ?assertEqual(
       ["Usher","UFC","Jessica Ghawi","Columbine","News",
	"British Open Leaderboard","Tom Hardy","Aurora",
	"Colorado shooting","James Holmes","Batman","Dwight Howard",
	"Ramadan 2012","Fred Willard","Emmy nominations 2012",
	"Michelle Jenneke","British Open","Chick Fil A","MC Chris",
	"AccuWeather"],
       gpop:poll_trends()).


-endif.

