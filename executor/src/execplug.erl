-module(execplug).
-behaviour(emp_plugin).
-include("emputil.hrl").
-include("emp_typedefs.hrl").

% Emp plugin required functions.
-export([register_cmds/0, register_events/0, registration_info/0, init/1, finish/1]).

% Emp plugin commands.
-export([add_exec/3,rm_exec/2,rename_exec/3,lst_execs/1,run_exec/3]).


%%
%% ===========================================================================
%% Public EMP Plugin API
%% ===========================================================================
%%

registration_info() ->
	{timerplug, 
	 [ {initial_cmd, <<"exec">>},   %User can use --target=exec
	   {local_global, global},      %Doesn't neeed a particular comp. (!local)
	   {sys_user, user}	            %User level plugin 
	 ]}.


register_cmds() -> 
	[
	 ?COMMAND( <<"add">>, fun add_exec/3, 
			   [ ?PARAM(<<"path">>,?EMP_STRING,null),
				 ?PARAM(<<"name">>,?EMP_STRING,null)
				 ], ?EMP_BOOLEAN),
	 ?COMMAND( <<"rm">>, fun rm_exec/2, 
			   [ ?PARAM(<<"name">>, ?EMP_STRING,null)
				 ], ?EMP_BOOLEAN ),
	 ?COMMAND( <<"rename">>, fun rename_exec/3, 
			   [ ?PARAM(<<"oldname">>, ?EMP_STRING, null),
				 ?PARAM(<<"newname">>, ?EMP_STRING, null)
				], ?EMP_BOOLEAN),
	 ?COMMAND( <<"lst">>, fun lst_execs/1, [], ?EMP_LIST(?EMP_STRING)),
	 ?COMMAND( <<"run">>, fun run_exec/3, 
			   [ ?PARAM(<<"name">>, ?EMP_STRING, null),
				 ?PARAM(<<"params">>,?EMP_LIST(?EMP_STRING), [])
				 ], ?EMP_BOOLEAN)
	 ].

register_events() -> [].


% Default init/finish functions
init  ( StateId ) ->
    emp_plugin:set_var(StateId, "execs", dict:new()), %TODO: erases all saved.
    ok.
finish( _StateId ) -> ok.

%%
%% ===========================================================================
%% Plugin Commands
%% ===========================================================================
%%

add_exec( StateId, Path, Name ) ->
    emplog:debug("Execplugin adding path to list!"),
	case emp_plugin:update_var( StateId, "execs",
								fun (OldState) -> 
									dict:store(Name, Path, OldState)
								end)
	of
		{ok,_} -> true;
		_      -> false
	end.

rm_exec( StateId, Name ) ->
    emplog:debug("Execplugin removing path to list!"),
	case emp_plugin:update_var(StateId, "execs",
							   fun (OldState) ->
									dict:erase(Name, OldState)
							   end)
	of
		{ok,_} -> true;
		_      -> false
	end.
		
lst_execs( StateId )->
    emplog:debug("listing programs"),
    case emp_plugin:get_var(StateId, "execs") of
        {ok, Execs} ->
        	lists:map( 
        		fun ({A,B})-> 
                         LA = binary_to_list(A),
                         LB = binary_to_list(B),
                         list_to_binary(lists:concat([LA," - ",LB])) end, 
        		dict:to_list(Execs));
        _ -> []
    end.


rename_exec(StateId, OldName, NewName)->
    emplog:debug("renaming programs"),
	case emp_plugin:update_var(StateId, "execs",
							   fun (OldState) ->
									Path = dict:fetch(OldName,OldState),
									dict:store(NewName, Path, dict:erase(OldName, OldState))
							   end)
	of
		{ok,_} -> true;
		_      -> false
	end.
	
run_exec(StateId, Name, Params)->
    emplog:debug("running!"),
    case emp_plugin:get_var(StateId, "execs") of
        {ok, Execs} ->
            emplog:debug("still running"),
        	case dict:find(Name, Execs) of
                {ok, Path} ->
                    emplog:debug("building command list!"),
                    Cmd = buildCmd([Path|Params]),
                    emplog:debug(io_lib:format("Execplugin running path: ~p",[Cmd])),
                	spawn(os, cmd, [Cmd]),
                	true;
                _ -> {error, <<"Unknown Program to execute">>}
            end;
        _ -> {error, <<"Couldn't find any programs!">>}
	end.

%%
%% ===========================================================================
%% Private functions
%% ===========================================================================
%%

%Builds a list from the arguments. It assumes each element of the list is a 
% binary, as that is what we recieve over the wire. 
-define(F, fun (X)-> binary_to_list(X)++[" "] end).
buildCmd( Args ) -> lists:flatten(lists:map(?F, Args)).


